import { document, console } from 'global';
import { storiesOf } from '@storybook/html';
import { withKnobs, text } from '@storybook/addon-knobs';

import '../app/css/fontawesome-all.min.css';

import { BasicScorepad } from '../app/js/basic-scorepad.component';

storiesOf('Scorepad', module)
  .addDecorator(withKnobs)
  .add('5 Players', () => {
    return /* html */ `
      <link href="https://fonts.googleapis.com/css?family=Bree+Serif&display=swap" rel="stylesheet">

      <basic-scorepad></basic-scorepad>`;
  });
