import { document, console } from 'global';
import { storiesOf } from '@storybook/html';
import { withKnobs, text } from '@storybook/addon-knobs';

import { ExampleElement } from '../app/js/example-element.component';
import { SimpleGreeting } from '../app/js/simple-greeting.component';

import '../app/js/react-example-1.component';
import '../app/js/react-example-2.component';
import '../app/js/react-example-3.component';
import '../app/js/react-example-4.component';

storiesOf('Vanilla JS', module)
  .addDecorator(withKnobs)
  .add('example-element', () => {
    return /* html */ `
      <example-element name="Everyone"></example-element>
    `;
  });

storiesOf('LitElement', module)
  .addDecorator(withKnobs)
  .add('simple-greeting', () => {
    const name = text('Name', 'Everyone');
    return /* html */ `
        <simple-greeting name="${name}"></simple-greeting>
        <p>This paragraph should NOT be red!</p>`;
  })
  .add('React Example 1', () => /* html */ `<hello-message name="Taylor"/>`)
  .add('React Example 2', () => /* html */ `<le-timer/>`)
  .add('React Example 3', () => /* html */ `<todo-app/>`)
  .add('React Example 4', () => /* html */ `<markdown-editor/>`);
