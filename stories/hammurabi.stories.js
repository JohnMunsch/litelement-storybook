import { document, console } from 'global';
import { storiesOf } from '@storybook/html';

import AboutPage from '../app/js/hammurabi/about-page.component';
import AccountComponent from '../app/js/hammurabi/account.component';
import ListingsPage from '../app/js/hammurabi/listings-page.component';
import ProfilePage from '../app/js/hammurabi/profile-page.component';

storiesOf('Hammurabi', module)
  .add('Listings Page', () => {
    let element = document.createElement('listings-page');
    element.model = {
      store: {
        getState: () => {},
        subscribe: () => {}
      }
    };

    return element;
  })
  .add('Profile Page', () => {
    let element = document.createElement('profile-page');
    element.model = {
      store: {
        getState: () => {
          return {
            account: {}
          };
        },
        subscribe: () => {}
      }
    };

    return element;
  })
  .add('About Page', () => /* html */ `<about-page></about-page>`)
  .add(
    'Account Component - Authenticated',
    () =>
      /* html */ `<account-component username="john.munsch@gmail.com"></account-component>`
  )
  .add(
    'Account Component - Unauthenticated',
    () => /* html */ `<account-component></account-component>`
  );
