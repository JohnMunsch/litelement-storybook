import { document, console } from 'global';
import { storiesOf } from '@storybook/html';

import '../app/js/peity-clone.component';

storiesOf('Peity', module)
  .add(
    'Pie Charts',
    () => /* html */ `<h2>Pie Charts</h2>
  <div>
    <le-pie>1/5</le-pie>
    <le-pie>226/360</le-pie>
    <le-pie>0.52/1.561</le-pie>
    <le-pie>1,4</le-pie>
    <le-pie>226,134</le-pie>
    <le-pie>0.52,1.041</le-pie>
    <le-pie>1,2,3,2,2</le-pie>
  </div>`
  )
  .add(
    'Donut Charts',
    () => /* html */ `<h2>Donut Charts</h2>
  <div>
    <le-donut>1/5</le-donut>
    <le-donut>226/360</le-donut>
    <le-donut>0.52/1.561</le-donut>
    <le-donut>1,4</le-donut>
    <le-donut>226,134</le-donut>
    <le-donut>0.52,1.041</le-donut>
    <le-donut>1,2,3,2,2</le-donut>
  </div>`
  )
  .add(
    'Line Charts',
    () => /* html */ `<h2>Line Charts</h2>
  <div>
    <le-line>5,3,9,6,5,9,7,3,5,2</le-line>
    <le-line>5,3,2,-1,-3,-2,2,3,5,2</le-line>
    <le-line>0,-3,-6,-4,-5,-4,-7,-3,-5,-2</le-line>
  </div>`
  )
  .add(
    'Bar Charts',
    () => /* html */ `<h2>Bar Charts</h2>
  <div>
    <le-bar>5,3,9,6,5,9,7,3,5,2</le-bar>
    <le-bar>5,3,2,-1,-3,-2,2,3,5,2</le-bar>
    <le-bar>0,-3,-6,-4,-5,-4,-7,-3,-5,-2</le-bar>
  </div>`
  )
  .add(
    'Attributes',
    () => /*html */ `
  <h2>Attributes</h2>
  <div>
    <le-donut fill='["red", "#eeeeee"]' inner-radius="10" radius="40">1/7</le-donut>
    <le-donut fill='["orange", "#eeeeee"]' inner-radius="14" radius="36">2/7</le-donut>
    <le-donut fill='["yellow", "#eeeeee"]' inner-radius="16" radius="32">3/7</le-donut>
    <le-donut fill='["green", "#eeeeee"]' inner-radius="18" radius="28">4/7</le-donut>
    <le-donut fill='["blue", "#eeeeee"]' inner-radius="20" radius="24">5/7</le-donut>
    <le-donut fill='["indigo", "#eeeeee"]' inner-radius="18" radius="20">6/7</le-donut>
    <le-donut fill='["violet", "#eeeeee"]' inner-radius="15" radius="16">7/7</le-donut>
  </div>`
  );
