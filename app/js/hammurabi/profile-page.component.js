import { LitElement, css, html } from 'lit-element';
import { unsafeHTML } from 'lit-html/directives/unsafe-html';

class ProfilePage extends LitElement {
  static get properties() {
    return {
      model: Object
    };
  }

  static get styles() {
    return css`
      /* @import url('https://fonts.googleapis.com/css?family=Open+Sans|Oswald'); */

      :host {
        display: block;
        max-width: 1200px;
        margin-left: auto;
        margin-right: auto;
        margin-bottom: 20px;

        --ivy-league: #607848;
        --mossociety: #789048;
        --agrio: #c0d860;
        --aglet: #f0f0d8;
        --purple: #604848;

        --header-background-color: var(--purple);
        --header-text-color: var(--agrio);
        --headline-text-color: var(--purple);

        font-family: 'Open Sans', sans-serif;
        font-size: smaller;
      }

      h1,
      h2,
      h3 {
        font-family: 'Oswald', sans-serif;
        color: var(--headline-text-color);
      }

      p,
      div {
        font-family: 'Open Sans', sans-serif;
      }
    `;
  }

  firstUpdated(changedProperties) {
    this.model.store.subscribe(() => {
      this.requestUpdate();
    });
  }

  _loggedIn(state) {
    if (state.account.userCollection && state.account.userWishlist) {
      return html`
        <h2>
          Wishlist
          <span class="edit-link"
            >(<a
              href="https://boardgamegeek.com/collection/user/${state.account
                .id}?wishlist=1&ff=1"
              >Edit your wishlist on BoardGameGeek</a
            >)</span
          >
        </h2>
        <bgg-item-list
          .items="${state.account.userWishlist.items.item}"
        ></bgg-item-list>

        <h2>
          Collection
          <span class="edit-link"
            >(<a
              href="https://boardgamegeek.com/collection/user/${state.account
                .id}?own=1&ff=1"
              >Edit your collection on BoardGameGeek</a
            >)</span
          >
        </h2>
        <bgg-item-list
          .items="${state.account.userCollection.items.item}"
        ></bgg-item-list>
      `;
    } else {
      return html``;
    }
  }

  _loggedOut(state) {
    return html``;
  }

  _changeHandler(e) {
    this.model.setIdToken(e.target.value.trim());
  }

  render() {
    if (this.model) {
      let state = this.model.store.getState();

      return html`
        <h1>Profile</h1>

        ${state.account.id ? this._loggedIn(state) : this._loggedOut(state)}
      `;
    } else {
      return html``;
    }
  }
}

class BGGItemListElement extends LitElement {
  static get properties() {
    return { items: Array };
  }

  _priorityDescription(priority) {
    switch (priority) {
      case 1:
        return 'Must have';
      case 2:
        return 'Love to have';
      case 3:
        return 'Like to have';
      case 4:
        return 'Thinking about it';
      case 5:
        return `Don't buy this`;
    }
  }

  _renderPriority(item) {
    if (item.status.wishlistpriority) {
      return html`
        -
        ${item.status.wishlistpriority}/${this._priorityDescription(
          item.status.wishlistpriority
        )}
      `;
    } else {
      return null;
    }
  }

  _renderItem(item) {
    return html`
      <li>
        <span class="name">${unsafeHTML(item.name.text)}</span>
        (${item.yearpublished})${this._renderPriority(item)}
      </li>
    `;
  }

  render() {
    if (this.items) {
      return html`
        <style>
          :host {
            display: block;
          }
          :host([hidden]) {
            display: none;
          }

          ul {
            list-style: none;
          }

          .name {
            font-weight: bold;
          }
        </style>

        <ul>
          ${this.items.map(item => this._renderItem(item))}
        </ul>
      `;
    } else {
      return html``;
    }
  }
}

customElements.define('profile-page', ProfilePage);
customElements.define('bgg-item-list', BGGItemListElement);
