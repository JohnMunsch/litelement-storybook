import { LitElement, css, html } from 'lit-element';

class AboutPage extends LitElement {
  static get styles() {
    return css`
      /* @import url('https://fonts.googleapis.com/css?family=Open+Sans|Oswald'); */

      :host {
        display: block;
        max-width: 1200px;
        margin-left: auto;
        margin-right: auto;
        margin-bottom: 20px;

        --ivy-league: #607848;
        --mossociety: #789048;
        --agrio: #c0d860;
        --aglet: #f0f0d8;
        --purple: #604848;

        --header-background-color: var(--purple);
        --header-text-color: var(--agrio);
        --headline-text-color: var(--purple);

        font-family: 'Open Sans', sans-serif;
        font-size: smaller;
      }

      h1,
      h2,
      h3 {
        font-family: 'Oswald', sans-serif;
        color: var(--headline-text-color);
      }

      p,
      div {
        font-family: 'Open Sans', sans-serif;
      }
    `;
  }

  render() {
    return html`
      <h1>About</h1>

      <p>
        The VFM index exists for the most obvious of reasons, which is that I
        wanted it for myself. The earliest versions had no UI at all, they just
        monitored the VFM and emailed me selected items when they showed up on
        the list. But the reason the current version exists is that I am using
        it as a fundraising vehicle for my favorite charity: Extra Life!
      </p>

      <p>
        It's run by the Children's Miracle Network and it raises money for
        non-profit children's hospitals (<a
          href="https://www.charitynavigator.org/index.cfm?bay=search.summary&orgid=5756"
          >ratings on Charity Navigator</a
        >).
      </p>

      <h3>Things You Might Want to do on the VFM</h3>

      <h4>I want to list an item for sale</h4>
      <ol>
        <li>Go to the geeklist in question: (link here)</li>
        <li>Log in to BGG so it will let you add an item.</li>
        <li>
          Click the “Add Item” button you see just below the VFM header: (image
          here)
        </li>
        <li>You’ll see a page like this: (image here)</li>
        <li>Pick the “Board Game” link: (image here)</li>
        <li>
          Type in the name of the game you want to list. If it is something
          which is not a board game in BGG’s database (dice, a dice tower,
          carrying bag, anything else on Earth), type in “Outside” and you
          should see a game named “Outside the scope of BGG”. Pick that item.
        </li>
        <li>
          If you need a different picture (for example, you have a different
          version than what the default picture shows), you can find it in the
          gallery and put that picture ID here: (Image here)
        </li>
        <li>
          Put in your description (if it is an unusual edition of the item,
          provide that info and describe the item’s condition) and a price (with
          a $ in front of the price please, for the indexer). You can use the
          formatting tools they provide to add images and link to expansions
          you’re including.
        </li>
        <li>
          If you are listing an “Outside the scope of BGG” item, you can add a
          chunk of text like this, "+altname+ Whatever You'd Like To Call Your
          Item -altname-", and the index will list it by that name for
          searching.
        </li>
      </ol>

      <h4>I want to bid on an item for sale</h4>
      <ol>
        <li>Follow the first two steps above.</li>
        <li>
          Find the item in the geek list you wish to bid on and click the
          “Comment” button.
        </li>
        <li>
          Enter in any question(s) you have (for example about edition,
          condition, or included components) or alternatively, list a price
          you’re willing to bid for the item.
        </li>
        <li>
          If the listing user accepts your bid, they will normally strikethrough
          all of the items description and put “Sold” on the item and list your
          name as the winning bidder.
        </li>
      </ol>

      <h4>I want to mark my item as sold</h4>
      <ol>
        <li>
          If you sell an item, add “Sold” to its description (geeklist items may
          be changed as often as necessary). Most users also add the name of the
          seller, final price, and often use strike through on the original
          description though none of that is a requirement. Do not delete your
          sold item from the list!
        </li>
      </ol>

      <h3>Acknowledgements</h3>
      <ul>
        <li>
          <a href="https://thenounproject.com/icon/516351/"
            >King icon courtesy of Nenad Radojcic @ The Noun Project</a
          >
        </li>
        <li>
          <a href="https://thenounproject.com/icon/1269/"
            >Meeple icon courtesy of Johan H. W. Basberg @ The Noun Project</a
          >
        </li>
      </ul>
    `;
  }
}

customElements.define('about-page', AboutPage);
