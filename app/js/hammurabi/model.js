export function _basicSort(field, a, b, op = value => value) {
  let aValue = op(a[field]);
  let bValue = op(b[field]);

  if (aValue < bValue) {
    return -1;
  }

  if (aValue > bValue) {
    return 1;
  }

  // a must be equal to b
  return 0;
}
