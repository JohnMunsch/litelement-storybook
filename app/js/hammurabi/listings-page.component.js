import { LitElement, css, html } from 'lit-element';
import { unsafeHTML } from 'lit-html/directives/unsafe-html';
import format from 'date-fns/format';

import { _basicSort } from './model';

class ListingsPage extends LitElement {
  static get properties() {
    return {
      model: Object,
      geeklist: Object,
      itemList: Array,
      geeklistId: Number
    };
  }

  static get styles() {
    return css`
      /* @import url('https://fonts.googleapis.com/css?family=Open+Sans|Oswald'); */

      :host {
        display: block;
        max-width: 1200px;
        margin-left: auto;
        margin-right: auto;
        margin-bottom: 20px;

        --ivy-league: #607848;
        --mossociety: #789048;
        --agrio: #c0d860;
        --aglet: #f0f0d8;
        --purple: #604848;

        --header-background-color: var(--purple);
        --header-text-color: var(--agrio);
        --headline-text-color: var(--purple);

        font-family: 'Open Sans', sans-serif;
        font-size: smaller;
      }

      h1,
      h2,
      h3 {
        font-family: 'Oswald', sans-serif;
        color: var(--headline-text-color);
      }

      p,
      div {
        font-family: 'Open Sans', sans-serif;
      }

      #idToken {
        width: 950px;
        height: 50px;
      }

      .note {
        font-size: small;
      }

      .geeklist-title {
        font-size: smaller;
      }

      form {
        font-family: 'Open Sans', sans-serif;
        font-size: smaller;
      }

      .count {
        font-weight: bold;
      }

      .listing-row {
        display: flex;
        flex-wrap: wrap;

        font-family: 'Open Sans', sans-serif;
      }

      item-listings .listing-row:nth-child(2n) {
        background-color: #dbf5fe66;
      }

      a {
        cursor: pointer;
      }

      .item-sold {
        text-decoration: line-through;
      }

      div.listings-page div.listing-row:nth-child(odd) {
        background-color: whitesmoke;
      }

      .in-collection,
      .in-collection a {
        color: darkgray;
      }

      .wishlist-priority-1 {
        background: radial-gradient(circle at top left, white 0, #ff2600 100%);
      }

      .wishlist-priority-2 {
        background: radial-gradient(circle at top left, white 0, #ff5233 100%);
      }

      .wishlist-priority-3 {
        background: radial-gradient(circle at top left, white 0, #ff7d66 100%);
      }

      .wishlist-priority-4 {
        background: radial-gradient(circle at top left, white 0, #ffa899 100%);
      }

      .wishlist-priority-5 {
        background: radial-gradient(circle at top left, white 0, #ffcfc7 100%);
      }

      .listing-column,
      .price-column,
      .user-column,
      .description-column {
        padding: 2px 10px 2px 10px;
        box-sizing: border-box;
      }

      .listing-column {
        width: 33%;
        max-width: 250px;
        box-sizing: border-box;
      }

      .listing-column a {
        margin-right: 5px;
      }

      .listing-column img {
        height: 12px;
      }

      .pure-button.button-xsmall {
        font-size: 50%;
      }

      .price-column {
        width: 33%;
        max-width: 100px;
        text-align: right;
        box-sizing: border-box;
      }

      .user-column {
        width: 33%;
        max-width: 100px;
        font-size: smaller;
        font-weight: bold;
        box-sizing: border-box;
      }

      .date-listed {
        font-size: x-small;
        font-weight: normal;
      }

      .description-column {
        overflow-x: hidden;
        vertical-align: top;
        font-size: smaller;
        box-sizing: border-box;
      }

      .description {
        height: 100%;
      }

      .description img {
        max-width: 100%;
      }

      ul li {
        font-family: 'Open Sans', sans-serif;
      }
    `;
  }

  firstUpdated(changedProperties) {
    this.model.store.subscribe(() => {
      let state = this.model.store.getState();

      if (this.geeklistId) {
        this.geeklist = state.geeklists[this.geeklistId];
      }

      if (this.geeklist) {
        this.itemList = this._annotate(
          this.geeklist.items,
          state.account.userCollection,
          state.account.userWishlist
        );

        this.itemList = this._filterAndSort(
          this.itemList,
          state.searchText,
          state.soldStatus,
          state.sortOrder,
          state.collectionStatus
        );
      }

      this.requestUpdate();
    });
  }

  _filterChanged(searchText) {
    this.model.setSearchText(searchText);
  }

  _soldStatusChanged(soldStatus) {
    this.model.setSoldStatus(soldStatus);
  }

  createRenderRoot() {
    return this;
  }

  _extraLifeOptions(state) {
    if (!state.account.id) {
      return html`
        <option>
          Filter to your collection and wishlist items if you're an Extra Life
          contributor! See the Account page.
        </option>
      `;
    } else {
      return html`
        <option
          value="inCollection"
          ?selected="${state.collectionStatus == 'inCollection'}"
          ?disabled="${!state.account.id}"
          >In My Collection</option
        >
        <option
          value="inWishlist"
          ?selected="${state.collectionStatus == 'inWishlist'}"
          ?disabled="${!state.account.id}"
          >In My Wishlist</option
        >
      `;
    }
  }

  render() {
    if (this.model && this.geeklist && this.itemList) {
      let state = this.model.store.getState();

      return html`
        <h1>${this.geeklist.title}</h1>

        <div>
          <form>
            <label>Sold Status: </label>
            <select @change="${e => this._changeSoldStatus(e)}">
              <option
                value="soldAndUnsold"
                ?selected="${state.soldStatus == 'soldAndUnsold'}"
                >Sold and Unsold</option
              >
              <option value="sold" ?selected="${state.soldStatus == 'sold'}"
                >Sold</option
              >
              <option value="unsold" ?selected="${state.soldStatus == 'unsold'}"
                >Unsold</option
              >
            </select>

            <label>Collection Status: </label>
            <select @change="${e => this._changeCollectionStatus(e)}">
              <option value="" ?selected="${state.collectionStatus == ''}"
                >All Items</option
              >
              ${this._extraLifeOptions(state)}
            </select>

            <input
              value="${state.searchText}"
              @input="${e => this._changeSearchText(e)}"
              placeholder="Filter by Listing or User"
            />
          </form>
        </div>

        <h2>Listings</h2>

        <div class="listing-row">
          <div class="listing-column">
            <a @click="${e => this._toggleListingSort(e)}">
              Listing
              <i class="fa ${this._listingSortStyling(state.sortOrder)}"></i>
            </a>
          </div>
          <div class="price-column">
            <a @click="${e => this._togglePriceSort(e)}">
              Price
              <i class="fa ${this._priceSortStyling(state.sortOrder)}"></i>
            </a>
          </div>
          <div class="user-column">
            <a @click="${e => this._toggleUserSort(e)}">
              User <i class="${this._userSortStyling(state.sortOrder)} fa"></i
            ></a>
            and
            <a @click="${e => this._toggleDateSort(e)}"
              >Date <i class="${this._dateSortStyling(state.sortOrder)} fa"></i
            ></a>
          </div>
          <div class="description-column">Description</div>
        </div>
        <item-listings
          .items="${this.itemList}"
          .geeklistid="${this.geeklistId}"
        ></item-listings>

        <h2>Stats</h2>
        <div>
          <span class="count">Listed</span>: ${this.geeklist.stats.numItems}
        </div>
        <div>
          <span class="count">Sold</span>: ${this.geeklist.stats.numSold}
        </div>
        <div>
          <span class="count">Unsold</span>: ${this.geeklist.stats.numUnsold}
        </div>

        <h3>Sold (by User)</h3>

        <sold-listings
          .items="${this.geeklist.stats.soldCounts}"
          .geeklistid="${this.geeklistId}"
        ></sold-listings>
      `;
    } else {
      return html``;
    }
  }

  _listingSortStyling(sortOrder) {
    if (sortOrder == 'alphaItem') {
      return 'fa-sort-up';
    } else if (sortOrder == 'alphaItemDesc') {
      return 'fa-sort-down';
    } else {
      return 'fa-sort';
    }
  }

  _priceSortStyling(sortOrder) {
    if (sortOrder == 'price') {
      return 'fa-sort-up';
    } else if (sortOrder == 'priceDesc') {
      return 'fa-sort-down';
    } else {
      return 'fa-sort';
    }
  }

  _userSortStyling(sortOrder) {
    if (sortOrder == 'alphaUser') {
      return 'fa-sort-up';
    } else if (sortOrder == 'alphaUserDesc') {
      return 'fa-sort-down';
    } else {
      return 'fa-sort';
    }
  }

  _dateSortStyling(sortOrder) {
    if (sortOrder == 'oldestFirst') {
      return 'fa-sort-up';
    } else if (sortOrder == 'newestFirst') {
      return 'fa-sort-down';
    } else {
      return 'fa-sort';
    }
  }

  _toggleListingSort() {
    let state = this.model.store.getState();

    // Update the sort order based on the current one.
    if (state.sortOrder == 'alphaItem') {
      this.model.setSortOrder('alphaItemDesc');
    } else if (state.sortOrder == 'alphaItemDesc') {
      this.model.setSortOrder('oldestFirst');
    } else {
      this.model.setSortOrder('alphaItem');
    }
  }

  _togglePriceSort() {
    let state = this.model.store.getState();

    // Update the sort order based on the current one.
    if (state.sortOrder == 'price') {
      this.model.setSortOrder('priceDesc');
    } else if (state.sortOrder == 'priceDesc') {
      this.model.setSortOrder('oldestFirst');
    } else {
      this.model.setSortOrder('price');
    }
  }

  _toggleUserSort() {
    let state = this.model.store.getState();

    // Update the sort order based on the current one.
    if (state.sortOrder == 'alphaUser') {
      this.model.setSortOrder('alphaUserDesc');
    } else if (state.sortOrder == 'alphaUserDesc') {
      this.model.setSortOrder('oldestFirst');
    } else {
      this.model.setSortOrder('alphaUser');
    }
  }

  _toggleDateSort() {
    let state = this.model.store.getState();

    // Update the sort order based on the current one.
    if (state.sortOrder == 'oldestFirst') {
      this.model.setSortOrder('newestFirst');
    } else if (state.sortOrder == 'newestFirst') {
      this.model.setSortOrder('oldestFirst');
    } else {
      this.model.setSortOrder('oldestFirst');
    }
  }

  _changeSoldStatus(e) {
    this.model.setSoldStatus(e.target.value);
  }

  _changeCollectionStatus(e) {
    this.model.setCollectionStatus(e.target.value);
  }

  _changeSearchText(e) {
    this.model.setSearchText(e.target.value);
  }

  _annotate(geeklist, collection, wishlist) {
    return geeklist.map(item => {
      if (collection) {
        if (
          collection.items.item.find(collectionItem => {
            return collectionItem.objectid == item.id;
          })
        ) {
          item.inCollection = true;
        }
      }

      if (wishlist) {
        let foundItem = wishlist.items.item.find(wishlistItem => {
          return wishlistItem.objectid == item.id;
        });

        if (foundItem) {
          item.inWishlist = true;
          item.wishlistPriority = foundItem.status.wishlistpriority;
        }
      }

      // TODO: Move this to the model and do it once when we get a new geeklist.
      item.htmlDescription = item.description
        .replace(/\[-\]/gi, '<s>')
        .replace(/\[\/-\]/gi, '</s>')
        .replace(/\[b\]/gi, '<b>')
        .replace(/\[\/b\]/gi, '</b>')
        .replace(/\[i\]/gi, '<em>')
        .replace(/\[\/i\]/gi, '</em>')
        .replace(/\[u\]/gi, '<u>')
        .replace(/\[\/u\]/gi, '</u>')
        .replace(/\[size=(\w+)\]/gi, '<span style="font-size: $1px">')
        .replace(/\[\/size\]/gi, '</span>')
        .replace(/\[color=(#\w+)\]/gi, '<span style="color: $1">')
        .replace(/\[\/color\]/gi, '</span>')
        .replace(/\[url=([^\]]+)\]/gi, '<a href="$1">')
        .replace(/\[\/url\]/gi, '</a>')
        .replace(
          /\[geeklist=(\w+)\]/gi,
          '<a href="https://www.boardgamegeek.com/geeklist/$1">Other BGG Geeklist</a>'
        )
        .replace(/\[\/geeklist\]/gi, '')
        .replace(
          /\[thing=(\d+)\]/gi,
          '<a href="https://www.boardgamegeek.com/boardgameexpansion/$1">Expansion</a>'
        )
        .replace(/\[\/thing\]/gi, '')
        .replace(
          /\[user=(\w+)\]/gi,
          '<a href="https://www.boardgamegeek.com/user/$1">$1</a>'
        )
        .replace(/\[\/user\]/gi, '')
        .replace(
          /\[imageid=(\w+)\s*\]/gi,
          '<a href="https://www.boardgamegeek.com/image/$1">BGG Image</a>'
        );

      return item;
    });
  }

  _chronologicalSort(a, b) {
    return _basicSort('index', a, b);
  }

  _alphaItemSort(a, b) {
    return _basicSort('name', a, b);
  }

  _alphaUserSort(a, b) {
    return _basicSort('user', a, b);
  }

  _priceSort(a, b) {
    return _basicSort('price', a, b);
  }

  _filterAndSort(
    geeklist,
    searchText,
    soldStatus,
    sortOrder,
    collectionStatus
  ) {
    let itemList = [];
    let searchRegExp;

    if (searchText) {
      searchRegExp = new RegExp(searchText, 'i');
    }

    // Filter the list of items.
    if (geeklist) {
      itemList = geeklist.filter(item => {
        let keep = true;

        if (searchRegExp) {
          keep =
            searchRegExp.test(item.nameWithoutDiacritics) ||
            searchRegExp.test(item.user);
        }

        // This prevents us from continuing to check items if we've already filtered them out for another reason.
        if (keep) {
          if (
            (soldStatus == 'sold' && !item.sold) ||
            (soldStatus == 'unsold' && item.sold)
          ) {
            keep = false;
          }
        }

        // Same as above. Don't keep filtering if we've already filtered it out.
        if (keep) {
          if (
            (collectionStatus == 'inCollection' && !item.inCollection) ||
            (collectionStatus == 'inWishlist' && !item.inWishlist)
          ) {
            keep = false;
          }
        }

        return keep;
      });

      switch (sortOrder) {
        case 'oldestFirst':
          return itemList.sort(this._chronologicalSort);
        case 'newestFirst':
          return itemList.sort(this._chronologicalSort).reverse();
        case 'alphaItem':
          return itemList.sort(this._alphaItemSort);
        case 'alphaItemDesc':
          return itemList.sort(this._alphaItemSort).reverse();
        case 'alphaUser':
          return itemList.sort(this._alphaUserSort);
        case 'alphaUserDesc':
          return itemList.sort(this._alphaUserSort).reverse();
        case 'price':
          return itemList.sort(this._priceSort);
        case 'priceDesc':
          return itemList.sort(this._priceSort).reverse();
      }
    }
  }
}

class ItemListings extends LitElement {
  static get properties() {
    return { items: { type: Array }, geeklistid: { type: Number } };
  }

  _priceNote(item) {
    if (item.auction) {
      return html`
        [Auc.]
      `;
    } else if (item.firm) {
      return html`
        [Firm]
      `;
    }
  }

  _formatListingLink(geeklistId, index) {
    return `http://www.boardgamegeek.com/geeklist/${geeklistId}/item/${index}#item${index}`;
  }

  _formatItemLink(id) {
    return `https://www.boardgamegeek.com/boardgame/${id}`;
  }

  _formatDate(date) {
    // Sep 4, 1986 8:30 PM
    return format(date, 'MMM D YYYY H:mm A');
  }

  _lineStyling(item) {
    let classes = ['listing-row'];

    if (item.sold) {
      classes.push('item-sold');
    }

    if (item.inCollection) {
      classes.push('in-collection');
    }

    if (item.inWishlist) {
      classes.push('in-wishlist');
      classes.push(`wishlist-priority-${item.wishlistPriority}`);
    }

    return classes.join(' ');
  }

  _renderItem(item, geeklistId) {
    return html`
      <div class="${this._lineStyling(item)}"">
        <div class="listing-column">
          <a href="${this._formatListingLink(geeklistId, item.index)}">
          ${item.name}</a>&nbsp;<a href="${this._formatItemLink(
      item.id
    )}"><img src="./img/meeple.svg"></a>
        </div>
        <div class="price-column">
          $${item.price}${this._priceNote(item)}
        </div>
        <div class="user-column">
          ${item.user}<br>
          <span class="date-listed">${this._formatDate(item.date)}</span>
        </div>
        <div class="description-column description">
          ${unsafeHTML(item.htmlDescription)}
        </div>
      </div>`;
  }

  createRenderRoot() {
    return this;
  }

  render() {
    return html`
      ${this.items.map(item => this._renderItem(item, this.geeklistid))}
    `;
  }
}

class SoldListings extends LitElement {
  static get properties() {
    return { items: { type: Array }, geeklistid: { type: Number } };
  }

  _renderItem(item, geeklistId) {
    return html`
      <li>
        ${item.user}:
        <a
          target="_blank"
          href="/hammurabi.html?soldStatus=sold&sortOrder=alphaUser&searchText=${item.user}#/listings/${geeklistId}"
        >
          ${item.sold}
        </a>
      </li>
    `;
  }

  createRenderRoot() {
    return this;
  }

  render() {
    return html`
      <ul>
        ${this.items.map(item => this._renderItem(item, this.geeklistid))}
      </ul>
    `;
  }
}

customElements.define('listings-page', ListingsPage);
customElements.define('item-listings', ItemListings);
customElements.define('sold-listings', SoldListings);
