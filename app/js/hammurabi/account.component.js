import { LitElement, css, html } from 'lit-element';

class AccountComponent extends LitElement {
  static get styles() {
    return css`
      /* @import url('https://fonts.googleapis.com/css?family=Open+Sans|Oswald'); */

      :host {
        display: block;

        font-family: 'Open Sans', sans-serif;
        font-size: smaller;
      }
    `;
  }

  static get properties() {
    return {
      username: {
        type: String
      }
    };
  }

  register(event) {
    let registerEvent = new CustomEvent('ac-register', {
      detail: { message: 'hello. a load-complete happened.' }
    });
    this.dispatchEvent(registerEvent);
  }

  signIn(event) {
    let signInEvent = new CustomEvent('ac-sign-in', {
      detail: { message: 'hello. a load-complete happened.' }
    });
    this.dispatchEvent(signInEvent);
  }

  signOut(event) {
    let signOutEvent = new CustomEvent('ac-sign-out', {});
    this.dispatchEvent(signOutEvent);
  }

  unauthenticated() {
    return html`
      <input></input>
      <input></input>
      <button @click="${this.signIn}">Sign In</button>/<button @click="${
      this.register
    }">Register</button>
      `;
  }

  authenticated() {
    return html`
      ${this.username}
      <button @click="${this.signOut}">Sign Out</button>
    `;
  }

  render() {
    if (this.username) {
      return this.authenticated();
    } else {
      return this.unauthenticated();
    }
  }
}

customElements.define('account-component', AccountComponent);
