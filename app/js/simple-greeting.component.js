import { LitElement, css, html } from 'lit-element';

export class SimpleGreeting extends LitElement {
  static get properties() {
    return { name: { type: String } };
  }

  static get styles() {
    return css`
      p {
        color: red;
      }
    `;
  }

  constructor() {
    super();
    this.name = 'World';
  }

  render() {
    return html`
      <p>Hello, ${this.name}!</p>
    `;
  }
}

customElements.define('simple-greeting', SimpleGreeting);
