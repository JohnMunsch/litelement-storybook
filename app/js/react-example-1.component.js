import { LitElement, html } from 'lit-element';

class HelloMessage extends LitElement {
  // Public property API that triggers re-render (synced with attributes)
  static get properties() {
    return {
      name: String
    };
  }

  // Render method should return a `TemplateResult` using the provided lit-html `html` tag function
  render() {
    return html`
      <div>
        Hello ${this.name}
      </div>
    `;
  }
}
customElements.define('hello-message', HelloMessage);
