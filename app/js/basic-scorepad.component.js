import { LitElement, css, html } from 'lit-element';

export class BasicScorepad extends LitElement {
  static get styles() {
    return css`
      :host {
      }

      p,
      div {
        font-family: 'Bree Serif', serif;
        font-size: 20px;
      }

      .player {
        display: grid;
        grid-template-columns: 1fr 30px 30px 30px;
        max-width: 175px;
      }

      :host .player:nth-child(odd) {
        background-color: lightblue;
      }

      .score {
        text-align: right;
        margin-right: 10px;
      }

      .control {
        text-align: center;
        background-color: lightgray;
        margin: 2px;
        cursor: pointer;
      }
    `;
  }

  constructor() {
    super();
    this.scorepad = [
      {
        id: 'John',
        score: -1
      },
      {
        id: 'Brian',
        score: 14
      },
      {
        id: 'Richard',
        score: 10
      },
      {
        id: 'Simon',
        score: 18
      },
      {
        id: 'David',
        score: 26
      }
    ];
  }

  decrement(event, player) {
    console.log(player);
  }

  increment(event, player) {
    console.log(player);
  }

  renderPlayer(player) {
    return html`
      <div class="player">
        <div>${player.id}</div>
        <div class="score">${player.score}</div>
        <div class="control" @click="${e => this.decrement(e, player)}">
          <i class="fas fa-minus"></i>
        </div>
        <div class="control" @click="${e => this.increment(e, player)}">
          <i class="fas fa-plus"></i>
        </div>
      </div>
    `;
  }

  render() {
    return html`
      ${this.scorepad.map(player => this.renderPlayer(player))}
    `;
  }
}

customElements.define('basic-scorepad', BasicScorepad);
