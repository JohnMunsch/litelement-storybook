import { LitElement, html } from 'lit-element';

class Timer extends LitElement {
  // Public property API that triggers re-render (synced with attributes)
  static get properties() {
    return {
      seconds: Number
    };
  }

  constructor() {
    super();

    this.seconds = 0;
  }

  tick() {
    this.seconds++;
  }

  firstUpdated() {
    this.interval = setInterval(() => this.tick(), 1000);
  }

  disconnectedCallback() {
    super.disconnectedCallback();
    clearInterval(this.interval);
  }

  // Render method should return a `TemplateResult` using the provided lit-html `html` tag function
  render() {
    return html`
      <div>
        Seconds: ${this.seconds}
      </div>
    `;
  }
}
customElements.define('le-timer', Timer);
