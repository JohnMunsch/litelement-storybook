import { LitElement, html } from 'lit-element';
import { unsafeHTML } from 'lit-html/directives/unsafe-html';
import * as Remarkable from 'remarkable';

class MarkdownEditor extends LitElement {
  // Public property API that triggers re-render (synced with attributes)
  static get properties() {
    return {
      value: String
    };
  }

  constructor() {
    super();

    this.md = new Remarkable();
    this.value = 'Hello, **world**!';
  }

  handleOnInput(e) {
    this.value = e.target.value;
  }

  getRawMarkup(value) {
    return this.md.render(value);
  }

  // Render method should return a `TemplateResult` using the provided lit-html `html` tag function
  render() {
    return html`
      <div className="MarkdownEditor">
        <h3>Input</h3>
        <label for="markdown-content">
          Enter some markdown
        </label>
        <textarea
          id="markdown-content"
          @input="${e => this.handleOnInput(e)}"
          .value="${this.value}"
        ></textarea>
        <h3>Output</h3>
        <div class="content">${unsafeHTML(this.getRawMarkup(this.value))}</div>
      </div>
    `;
  }
}
customElements.define('markdown-editor', MarkdownEditor);
