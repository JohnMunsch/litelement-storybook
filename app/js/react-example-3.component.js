import { LitElement, html } from 'lit-element';

class TodoApp extends LitElement {
  // Public property API that triggers re-render (synced with attributes)
  static get properties() {
    return {
      items: Array,
      text: String
    };
  }

  constructor() {
    super();

    this.items = [];
    this.text = '';
  }

  handleOnChange(e) {
    this.text = e.target.value;
  }

  handleOnSubmit(e) {
    e.preventDefault();

    if (!this.text.length) {
      return;
    }

    const newItem = {
      text: this.text,
      id: Date.now()
    };

    this.items = this.items.concat(newItem);
  }

  // Render method should return a `TemplateResult` using the provided lit-html `html` tag function
  render() {
    return html`
      <div>
        <h3>TODO</h3>
        <todo-list .items="${this.items}"></todo-list>
        <form @submit="${e => this.handleOnSubmit(e)}">
          <label for="new-todo">
            What needs to be done?
          </label>
          <input
            id="new-todo"
            @change="${e => this.handleOnChange(e)}"
            value="${this.text}"
          />
          <button>
            Add #${this.items.length + 1}
          </button>
        </form>
      </div>
    `;
  }
}
customElements.define('todo-app', TodoApp);

class TodoList extends LitElement {
  static get properties() {
    return {
      items: Array
    };
  }

  render() {
    return html`
      <ul>
        ${this.items.map(
          item =>
            html`
              <li key="${item.id}">${item.text}</li>
            `
        )}
      </ul>
    `;
  }
}
customElements.define('todo-list', TodoList);
